# <p align="center"><strong> FlyFlow </strong></p>
## <p align="center"><strong>高效自动化的工作流程解决方案</strong></p>
<p align="center">
      <img src="https://img.shields.io/badge/Vue-3.4.3-orange" alt="Vue3"/>
    <img src="https://img.shields.io/badge/ElementPlus-2.4.3-orange" alt="Vue3"/>

</p>

### 概述

FlyFlow借鉴了钉钉与飞书的界面设计理念，致力于打造一款用户友好、快速上手的工作流程工具。相较于传统的基于BPMN.js的工作流引擎，我们提供的解决方案显著简化了操作逻辑，使得用户能够在极短的时间内构建定制化的业务流程，即便是不具备深厚技术背景的普通用户也能迅速掌握，实现零门槛的高效工作流配置。

感谢[vue3-element-admin](https://gitee.com/youlaiorg/vue3-element-admin "vue3-element-admin")和[Workflow-Vue3](https://github.com/StavinLi/Workflow-Vue3 "Workflow-Vue3")的作者无私奉献，使我能够借助这两个优秀的产品，轻松搭建了前端管理后台框架和流程图，节省了大量时间和精力，再次特别感谢！
> 后端代码开源地址：[https://gitee.com/junyue/flyflow](https://gitee.com/junyue/flyflow "https://gitee.com/junyue/flyflow")

### 官网-文档

[https://www.flyflow.cc](https://www.flyflow.cc "https://www.flyflow.cc")

![msedge_mNNAqkgNd9.png](imgs/msedge_mNNAqkgNd9.png)


### 技术架构
前端：Vue3+ElementPlus

后端分为两个版本：

1. SpringBoot2.7.6(jdk8)+Mysql8+MybatisPlus+Flowable6.8.0+Hutool+SaToken+Anyline
2. SpringBoot3.2.5(jdk17)+Mysql8+MybatisPlus+Flowable7.0.1+Hutool+SaToken+Anyline

### 技术交流群

添加作者个人微信进群`cxygzl666`备注：`flyflow`

### 关于开源协议以及补充协议

#### 1、本项目如果个人使用完全免费，不受任何影响
#### 2、禁止以商业化方式再次出售分享本项目相关源代码
#### 3、允许商业化出售编译后的前端和后端代码
#### 4、本项目已申请软件著作权，尊重开源产品，尊重作者付出的劳动成果
#### 5. 不得将本软件应用于危害国家安全、荣誉和利益的行为，不能以任何形式用于非法为目的的行为
#### 6. 任何基于本软件而产生的一切法律纠纷和责任，均于作者无关




